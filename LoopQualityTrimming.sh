#!/bin/sh
LargeFile="104_.notCombined_2.fastq"
SmallFile="104_.q20-notCombined_2.fastq"
UniqueInstrumentName="D8WCT8Q1:96:"
OutFile="104_.q20-notCombined_2.sub.fastq"

echo "Reading infiles"
grep ${UniqueInstrumentName} ${LargeFile} > temp1
echo "Done reading file1"
grep ${UniqueInstrumentName} ${SmallFile} > temp2
echo "Done reading file2"
echo "Sorting infiles"
sort temp1 > temp3
sort temp2 > temp4
echo "Finding unique headers"
comm -23 temp3 temp4 > temp5
echo "Processing header name file"
sed -i 's/^@//g' temp5
tr ' ' \\t < temp5 > temp6
cut -f1 temp6 > temp7

echo "Parsing sequences from infile"
#Change the following path depending on where seqtk is installed
export PATH="/Users/Fernandolab/Sanjay/seqtk/:$PATH"
seqtk subseq ${LargeFile} temp7 > ${OutFile}
echo "Task completed"
echo "Removing temporary files"
rm temp*